# -*- coding: utf-8 -*- 

import socket
import threading
import sys


HOST = '127.0.0.1'
PORT = 8080 
ADDR = (HOST,PORT)

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def getstr():
	udata = ""
	data = conn.recv(1024)
	if data:
		udata = data.decode("utf-8")
	return udata

def sendto(msg):
	conn.send(msg.encode("utf-8"))

def workwithRECV():
	while 1:
		msg = getstr()
		if(msg == ""):
			conn.close()
			print "Connection with SERVER closed"
			sys.exit(0)
			break
		print "SERVER: " + msg

def main():
	try:
		conn.connect(ADDR)
	except:
		print "Cant connect to SERVER"
		return
	print "Connected to SERVER"
	_id = raw_input("id: ").decode("utf-8")
	conn.send(_id.encode("utf-8"))

	ts = threading.Thread(target=workwithRECV, args=())
	ts.daemon = True
	ts.start()

	while 1:
		msg = raw_input().decode("utf-8")
		if (msg == 'quit'):
			conn.close()
			break
		else:
			conn.send(msg.encode("utf-8"))

main()